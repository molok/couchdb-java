package pojo;

import org.codehaus.jackson.annotate.JsonProperty;


public class Client {
	
	private String name;
	private String address;
	private String id;
	private String revision;
	@JsonProperty("type")
	private String type = Client.class.getSimpleName();
	
	public Client(){
		
	}
	
	public Client(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@JsonProperty("_id")
	public String getId() {
		return id;
	}
	@JsonProperty("_id")
	public void setId(String id) {
		this.id = id;
	}
	@JsonProperty("_rev")
	public String getRevision() {
		return revision;
	}
	@JsonProperty("_rev")
	public void setRevision(String revision) {
		this.revision = revision;
	}

}
