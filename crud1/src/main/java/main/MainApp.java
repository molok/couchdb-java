package main;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.ektorp.AttachmentInputStream;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DbInfo;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pojo.Client;
import pojo.Order;

public class MainApp {
	
	private static Logger logger = LoggerFactory.getLogger(MainApp.class);
	
	private static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	private static void waitForKey(){
		try {input.readLine();}catch (IOException e) {}
	}
	
	
	
	
	

	public static void main(String[] args) throws MalformedURLException {
		
		HttpClient couchClient = new StdHttpClient.Builder()
								.username("admin")
								.password("admin")
								.url("http://localhost:5984")
								.build();
		
		CouchDbInstance dbInstance = new StdCouchDbInstance(couchClient);
		CouchDbConnector db = dbInstance.createConnector("testdb", true);
		
		DbInfo info = db.getDbInfo();
		DecimalFormat df = new DecimalFormat("#.##");

		logger.info("DB info : dbName=" + info.getDbName() + ", docCount="
				+ info.getDocCount() + ", diskSize=" + df.format(info.getDiskSize()
				/ 1024.0 / 1024.0) + " MB");

		Client john = new Client("John Smith", "USA");
		
		john.setId("john");
		
		logger.info("john : _id="+john.getId()+" _rev="+john.getRevision());
		waitForKey();		
		
		
		db.create(john);		
		logger.info("create");
		logger.info("john : _id="+john.getId()+" _rev="+john.getRevision());
		
		waitForKey();
		
		john.setAddress("PL");
		db.update(john);
		
		logger.info("update");
		logger.info("john : _id="+john.getId()+" _rev="+john.getRevision());
		
		waitForKey();
		
		logger.info("get");
		john = db.get(Client.class, "john");
		logger.info("john : _id="+john.getId()+" _rev="+john.getRevision());
		
		waitForKey();
		
		logger.info("delete");
		db.delete(john);
		
		waitForKey();
		
		logger.info("Order create");
		
		Order o1 = new Order();
		o1.setCustomerId(john.getId());
		db.create(o1);
		
		waitForKey();
		
		logger.info("Order delete ");
		
		db.delete(o1);
		
		waitForKey();
		
		logger.info("Map object");
		
		Map<String,Object> mapExample = new HashMap<>();
		mapExample.put("_id", "mapExample");
		mapExample.put("someNumber", 2);
		mapExample.put("table", new Integer[]{2,3,4,5});
		
		db.create(mapExample);
		
		waitForKey();
		
		logger.info("JsonNode");
		JsonNode json = db.get(JsonNode.class, "mapExample");
		String fields="";
		Iterator<String> it = json.getFieldNames();
		while(it.hasNext()){
			fields+=" :: "+it.next();
		}
		logger.info("fields = "+fields);
		
	}

}
