jaki by� �redni czas realizacji zam�wienia w ka�dym roku
Year: 1999, Average time: 3.5481927710843375 days
Year: 2000, Average time: 3.529230769230769 days
Year: 2001, Average time: 3.515177065767284 days
Year: 2002, Average time: 3.5023944275141496 days
Year: 2003, Average time: 3.515739542906425 days
Year: 2004, Average time: 3.4997813729776994 days
Year: 2005, Average time: 3.469206911829862 days
Year: 2006, Average time: 3.531705227077977 days
Year: 2007, Average time: 3.486825053995681 days
Year: 2008, Average time: 3.532336119665641 days
Year: 2009, Average time: 3.5126446635233606 days
Year: 2010, Average time: 3.502355460385441 days
Year: 2011, Average time: 3.497808939526731 days
Year: 2012, Average time: 3.4818435754189956 days
Year: 2013, Average time: 5.142857142857143 days

jaka by�a �rednia warto�� jednej sztuki produktu dla ka�dego ze spedytor�w w ka�dym roku 
Shipper id="1", year=1999, avgUnitPrice=86.91130434782607
Shipper id="1", year=2000, avgUnitPrice=84.21328070175439
Shipper id="1", year=2001, avgUnitPrice=85.01583972719523
Shipper id="1", year=2002, avgUnitPrice=84.6321632996633
Shipper id="1", year=2003, avgUnitPrice=83.44356464270965
Shipper id="1", year=2004, avgUnitPrice=85.46879326047357
Shipper id="1", year=2005, avgUnitPrice=84.7039836347976
Shipper id="1", year=2006, avgUnitPrice=83.52862365591395
Shipper id="1", year=2007, avgUnitPrice=84.34666255144035
Shipper id="1", year=2008, avgUnitPrice=85.87748611111111
Shipper id="1", year=2009, avgUnitPrice=85.56867678044668
Shipper id="1", year=2010, avgUnitPrice=84.11697754749568
Shipper id="1", year=2011, avgUnitPrice=84.82395031055901
Shipper id="1", year=2012, avgUnitPrice=86.01026773761714
Shipper id="1", year=2013, avgUnitPrice=128.60833333333335
Shipper id="2", year=1999, avgUnitPrice=86.13471698113206
Shipper id="2", year=2000, avgUnitPrice=84.63994199018296
Shipper id="2", year=2001, avgUnitPrice=85.60053630363036
Shipper id="2", year=2002, avgUnitPrice=85.87436570428696
Shipper id="2", year=2003, avgUnitPrice=83.42018222222222
Shipper id="2", year=2004, avgUnitPrice=85.81775074690569
Shipper id="2", year=2005, avgUnitPrice=84.4277329749104
Shipper id="2", year=2006, avgUnitPrice=84.63289029535868
Shipper id="2", year=2007, avgUnitPrice=83.29273550724638
Shipper id="2", year=2008, avgUnitPrice=85.12058260507696
Shipper id="2", year=2009, avgUnitPrice=85.55426426426428
Shipper id="2", year=2010, avgUnitPrice=84.49446229913471
Shipper id="2", year=2011, avgUnitPrice=84.89554791380098
Shipper id="2", year=2012, avgUnitPrice=86.32601941747573
Shipper id="2", year=2013, avgUnitPrice=98.56166666666667
Shipper id="3", year=1999, avgUnitPrice=87.42009950248757
Shipper id="3", year=2000, avgUnitPrice=85.09617187500002
Shipper id="3", year=2001, avgUnitPrice=85.11247229326514
Shipper id="3", year=2002, avgUnitPrice=85.38787348586811
Shipper id="3", year=2003, avgUnitPrice=85.28126421697289
Shipper id="3", year=2004, avgUnitPrice=86.04125753660638
Shipper id="3", year=2005, avgUnitPrice=85.07571041948577
Shipper id="3", year=2006, avgUnitPrice=84.63427394885132
Shipper id="3", year=2007, avgUnitPrice=82.43242306025142
Shipper id="3", year=2008, avgUnitPrice=84.73372783687942
Shipper id="3", year=2009, avgUnitPrice=84.3180697167756
Shipper id="3", year=2010, avgUnitPrice=83.90090185676395
Shipper id="3", year=2011, avgUnitPrice=85.5109777777778
Shipper id="3", year=2012, avgUnitPrice=84.73254411764704
Shipper id="3", year=2013, avgUnitPrice=103.96888888888888

ile zam�wie� z ka�dego kraju zosta�o zrealizowanych
Country: Argentina, Completed Orders: 551
Country: Austria, Completed Orders: 1492
Country: Belgium, Completed Orders: 644
Country: Brazil, Completed Orders: 3091
Country: Canada, Completed Orders: 1063
Country: Denmark, Completed Orders: 684
Country: Finland, Completed Orders: 825
Country: France, Completed Orders: 2838
Country: Germany, Completed Orders: 4385
Country: Ireland, Completed Orders: 694
Country: Italy, Completed Orders: 989
Country: Mexico, Completed Orders: 1027
Country: Norway, Completed Orders: 225
Country: Poland, Completed Orders: 247
Country: Portugal, Completed Orders: 463
Country: Spain, Completed Orders: 796
Country: Sweden, Completed Orders: 1343
Country: Switzerland, Completed Orders: 644
Country: United Kingdom, Completed Orders: 1984
Country: United States, Completed Orders: 4375
Country: Venezuela, Completed Orders: 1640

ile sztuk produkt�w od ka�dego z dostawc�w uda�o si� sprzeda�
SupplierId: 17 , quantity: 2130770
SupplierId: 18 , quantity: 1413310
SupplierId: 15 , quantity: 2133040
SupplierId: 16 , quantity: 2231150
SupplierId: 13 , quantity: 674590
SupplierId: 14 , quantity: 2198650
SupplierId: 11 , quantity: 2112930
SupplierId: 12 , quantity: 3474400
SupplierId: 21 , quantity: 1410680
SupplierId: 20 , quantity: 2082710
SupplierId: 22 , quantity: 1394630
SupplierId: 23 , quantity: 2108830
SupplierId: 24 , quantity: 2074380
SupplierId: 25 , quantity: 1476070
SupplierId: 26 , quantity: 1399580
SupplierId: 27 , quantity: 711780
SupplierId: 28 , quantity: 1408490
SupplierId: 29 , quantity: 1380990
SupplierId: 3 , quantity: 2254100
SupplierId: 2 , quantity: 2853220
SupplierId: 10 , quantity: 712700
SupplierId: 1 , quantity: 2156100
SupplierId: 7 , quantity: 3507370
SupplierId: 6 , quantity: 2091510
SupplierId: 5 , quantity: 1446210
SupplierId: 4 , quantity: 2112160
SupplierId: 9 , quantity: 1387180
SupplierId: 8 , quantity: 2848370

jaka kwota zam�wie� by�a zg�aszana w ka�dy z dni tygodnia
WeekDay=Mon, sumValue=6.5018005870000005E7
WeekDay=Tue, sumValue=6.755846189E7
WeekDay=Wed, sumValue=6.600827915000001E7
WeekDay=Thu, sumValue=6.685840337E7
WeekDay=Fri, sumValue=6.683447698999998E7
WeekDay=Sat, sumValue=6.5490946150000006E7

jaka by�a warto�� produkt�w zam�wionych z ka�dego z kraj�w w ka�dym roku; chodzi o kraj zamawiaj�cego
Country="Argentina", year=1999, sumValue=135583.32
Country="Argentina", year=2000, sumValue=607141.12
Country="Argentina", year=2001, sumValue=834411.48
Country="Argentina", year=2002, sumValue=503778.20999999996
Country="Argentina", year=2003, sumValue=638513.9299999999
Country="Argentina", year=2004, sumValue=601008.1599999999
Country="Argentina", year=2005, sumValue=506668.86
Country="Argentina", year=2006, sumValue=457492.58999999997
Country="Argentina", year=2007, sumValue=724128.48
Country="Argentina", year=2008, sumValue=1077437.3599999999
Country="Argentina", year=2009, sumValue=646394.79
Country="Argentina", year=2010, sumValue=607052.8099999999
Country="Argentina", year=2011, sumValue=749438.88
Country="Argentina", year=2012, sumValue=577729.82
Country="Austria", year=1999, sumValue=76365.56000000001
Country="Austria", year=2000, sumValue=1720817.5899999999
Country="Austria", year=2001, sumValue=1927977.9100000001
Country="Austria", year=2002, sumValue=1710137.9899999998
Country="Austria", year=2003, sumValue=1472794.5599999998
Country="Austria", year=2004, sumValue=1723016.8199999998
Country="Austria", year=2005, sumValue=1488337.61
Country="Austria", year=2006, sumValue=1705571.1400000001
Country="Austria", year=2007, sumValue=1949076.8899999997
Country="Austria", year=2008, sumValue=1939258.2
Country="Austria", year=2009, sumValue=1865629.2899999998
Country="Austria", year=2010, sumValue=2059774.5699999998
Country="Austria", year=2011, sumValue=1687072.6299999997
Country="Austria", year=2012, sumValue=1553682.3699999999
Country="Belgium", year=1999, sumValue=41271.40000000001
Country="Belgium", year=2000, sumValue=701902.2399999999
Country="Belgium", year=2001, sumValue=818147.5700000002
Country="Belgium", year=2002, sumValue=485243.7
Country="Belgium", year=2003, sumValue=783177.4900000001
Country="Belgium", year=2004, sumValue=869631.2000000002
Country="Belgium", year=2005, sumValue=664947.2600000001
Country="Belgium", year=2006, sumValue=775124.3500000001
Country="Belgium", year=2007, sumValue=799265.3500000001
Country="Belgium", year=2008, sumValue=786430.5299999999
Country="Belgium", year=2009, sumValue=970475.51
Country="Belgium", year=2010, sumValue=782074.32
Country="Belgium", year=2011, sumValue=775263.97
Country="Belgium", year=2012, sumValue=857216.09
Country="Brazil", year=1999, sumValue=306544.97000000003
Country="Brazil", year=2000, sumValue=3242929.5000000005
Country="Brazil", year=2001, sumValue=3932291.18
Country="Brazil", year=2002, sumValue=3593275.4799999995
Country="Brazil", year=2003, sumValue=3601441.2500000005
Country="Brazil", year=2004, sumValue=3233296.8000000007
Country="Brazil", year=2005, sumValue=3796369.62
Country="Brazil", year=2006, sumValue=3719408.55
Country="Brazil", year=2007, sumValue=3724981.56
Country="Brazil", year=2008, sumValue=3752359.6
Country="Brazil", year=2009, sumValue=3777665.7399999993
Country="Brazil", year=2010, sumValue=4160424.7299999986
Country="Brazil", year=2011, sumValue=3751287.39
Country="Brazil", year=2012, sumValue=3325661.6700000004
Country="Brazil", year=2013, sumValue=50401.67
Country="Canada", year=1999, sumValue=66812.66
Country="Canada", year=2000, sumValue=1355615.2900000003
Country="Canada", year=2001, sumValue=1486355.96
Country="Canada", year=2002, sumValue=1426818.28
Country="Canada", year=2003, sumValue=1156646.5
Country="Canada", year=2004, sumValue=1043909.0199999999
Country="Canada", year=2005, sumValue=1337923.27
Country="Canada", year=2006, sumValue=1173277.1
Country="Canada", year=2007, sumValue=1126504.01
Country="Canada", year=2008, sumValue=1237099.0799999998
Country="Canada", year=2009, sumValue=1209811.2000000002
Country="Canada", year=2010, sumValue=1350306.4299999997
Country="Canada", year=2011, sumValue=1073700.32
Country="Canada", year=2012, sumValue=1193552.8699999999
Country="Canada", year=2013, sumValue=52670.189999999995
Country="Denmark", year=1999, sumValue=96118.53
Country="Denmark", year=2000, sumValue=744067.11
Country="Denmark", year=2001, sumValue=860220.8499999999
Country="Denmark", year=2002, sumValue=876909.22
Country="Denmark", year=2003, sumValue=674594.2799999999
Country="Denmark", year=2004, sumValue=991280.74
Country="Denmark", year=2005, sumValue=674505.04
Country="Denmark", year=2006, sumValue=763841.06
Country="Denmark", year=2007, sumValue=863441.02
Country="Denmark", year=2008, sumValue=822985.4899999999
Country="Denmark", year=2009, sumValue=700714.68
Country="Denmark", year=2010, sumValue=986468.64
Country="Denmark", year=2011, sumValue=597547.7000000001
Country="Denmark", year=2012, sumValue=703518.3300000001
Country="Finland", year=1999, sumValue=72911.26
Country="Finland", year=2000, sumValue=714726.39
Country="Finland", year=2001, sumValue=1059591.62
Country="Finland", year=2002, sumValue=849596.34
Country="Finland", year=2003, sumValue=837693.27
Country="Finland", year=2004, sumValue=1218474.6300000001
Country="Finland", year=2005, sumValue=1003363.88
Country="Finland", year=2006, sumValue=1142833.1
Country="Finland", year=2007, sumValue=1049678.9000000001
Country="Finland", year=2008, sumValue=890285.37
Country="Finland", year=2009, sumValue=988497.01
Country="Finland", year=2010, sumValue=1043055.2700000003
Country="Finland", year=2011, sumValue=998623.1599999999
Country="Finland", year=2012, sumValue=761690.6299999999
Country="France", year=1999, sumValue=121687.34999999998
Country="France", year=2000, sumValue=3505699.02
Country="France", year=2001, sumValue=3380399.6799999997
Country="France", year=2002, sumValue=3862897.66
Country="France", year=2003, sumValue=3286707.120000001
Country="France", year=2004, sumValue=3290400.0100000002
Country="France", year=2005, sumValue=3670690.57
Country="France", year=2006, sumValue=2842549.1900000004
Country="France", year=2007, sumValue=3339888.38
Country="France", year=2008, sumValue=3375940.6900000004
Country="France", year=2009, sumValue=3373016.7399999998
Country="France", year=2010, sumValue=3249495.5000000005
Country="France", year=2011, sumValue=3349032.5100000007
Country="France", year=2012, sumValue=3231705.3400000003
Country="France", year=2013, sumValue=15942.29
Country="Germany", year=1999, sumValue=324925.1799999999
Country="Germany", year=2000, sumValue=4794169.83
Country="Germany", year=2001, sumValue=4905191.47
Country="Germany", year=2002, sumValue=5600143.979999999
Country="Germany", year=2003, sumValue=5732825.55
Country="Germany", year=2004, sumValue=4977867.899999999
Country="Germany", year=2005, sumValue=5199477.94
Country="Germany", year=2006, sumValue=5385220.46
Country="Germany", year=2007, sumValue=4566022.36
Country="Germany", year=2008, sumValue=5226866.809999999
Country="Germany", year=2009, sumValue=5578799.639999999
Country="Germany", year=2010, sumValue=5456582.970000001
Country="Germany", year=2011, sumValue=5187591.07
Country="Germany", year=2012, sumValue=5401997.760000003
Country="Germany", year=2013, sumValue=49728.37
Country="Ireland", year=1999, sumValue=116628.14000000001
Country="Ireland", year=2000, sumValue=841148.2
Country="Ireland", year=2001, sumValue=573133.28
Country="Ireland", year=2002, sumValue=1147541.4500000002
Country="Ireland", year=2003, sumValue=983019.59
Country="Ireland", year=2004, sumValue=780489.7500000001
Country="Ireland", year=2005, sumValue=703415.5700000001
Country="Ireland", year=2006, sumValue=914755.0499999998
Country="Ireland", year=2007, sumValue=687681.5299999999
Country="Ireland", year=2008, sumValue=829545.48
Country="Ireland", year=2009, sumValue=934513.81
Country="Ireland", year=2010, sumValue=690618.5900000001
Country="Ireland", year=2011, sumValue=773527.1299999999
Country="Ireland", year=2012, sumValue=634100.5099999998
Country="Italy", year=1999, sumValue=49343.03
Country="Italy", year=2000, sumValue=1133682.79
Country="Italy", year=2001, sumValue=1067167.0
Country="Italy", year=2002, sumValue=1217786.6500000001
Country="Italy", year=2003, sumValue=1182680.7100000002
Country="Italy", year=2004, sumValue=1241030.35
Country="Italy", year=2005, sumValue=999396.3799999999
Country="Italy", year=2006, sumValue=1189455.23
Country="Italy", year=2007, sumValue=1113318.2199999997
Country="Italy", year=2008, sumValue=1102761.87
Country="Italy", year=2009, sumValue=1416415.22
Country="Italy", year=2010, sumValue=1045361.0
Country="Italy", year=2011, sumValue=1147561.0299999998
Country="Italy", year=2012, sumValue=957632.63
Country="Mexico", year=1999, sumValue=75618.36
Country="Mexico", year=2000, sumValue=1326209.0699999998
Country="Mexico", year=2001, sumValue=1204365.83
Country="Mexico", year=2002, sumValue=1056838.92
Country="Mexico", year=2003, sumValue=1267064.33
Country="Mexico", year=2004, sumValue=1255205.3900000001
Country="Mexico", year=2005, sumValue=1220970.78
Country="Mexico", year=2006, sumValue=1240042.7000000002
Country="Mexico", year=2007, sumValue=1480400.9199999997
Country="Mexico", year=2008, sumValue=1009852.1099999999
Country="Mexico", year=2009, sumValue=1079599.08
Country="Mexico", year=2010, sumValue=1332210.19
Country="Mexico", year=2011, sumValue=1141434.78
Country="Mexico", year=2012, sumValue=1049066.52
Country="Norway", year=1999, sumValue=35979.979999999996
Country="Norway", year=2000, sumValue=363851.11
Country="Norway", year=2001, sumValue=447520.15
Country="Norway", year=2002, sumValue=127984.38999999998
Country="Norway", year=2003, sumValue=162801.56999999998
Country="Norway", year=2004, sumValue=321716.72
Country="Norway", year=2005, sumValue=334719.47
Country="Norway", year=2006, sumValue=255259.06
Country="Norway", year=2007, sumValue=254842.03
Country="Norway", year=2008, sumValue=308440.58999999997
Country="Norway", year=2009, sumValue=208786.27999999997
Country="Norway", year=2010, sumValue=219838.68999999994
Country="Norway", year=2011, sumValue=162759.6
Country="Norway", year=2012, sumValue=217333.99000000002
Country="Poland", year=1999, sumValue=33739.64
Country="Poland", year=2000, sumValue=308584.98
Country="Poland", year=2001, sumValue=284978.77
Country="Poland", year=2002, sumValue=321146.57
Country="Poland", year=2003, sumValue=363854.81999999995
Country="Poland", year=2004, sumValue=399788.51
Country="Poland", year=2005, sumValue=139621.37000000002
Country="Poland", year=2006, sumValue=255465.50999999995
Country="Poland", year=2007, sumValue=228194.30999999997
Country="Poland", year=2008, sumValue=278113.31
Country="Poland", year=2009, sumValue=243184.76000000007
Country="Poland", year=2010, sumValue=243997.21000000005
Country="Poland", year=2011, sumValue=320004.0
Country="Poland", year=2012, sumValue=175721.49
Country="Portugal", year=1999, sumValue=44659.979999999996
Country="Portugal", year=2000, sumValue=531749.35
Country="Portugal", year=2001, sumValue=714429.94
Country="Portugal", year=2002, sumValue=457684.63
Country="Portugal", year=2003, sumValue=552612.36
Country="Portugal", year=2004, sumValue=628183.19
Country="Portugal", year=2005, sumValue=602426.4700000001
Country="Portugal", year=2006, sumValue=523417.33
Country="Portugal", year=2007, sumValue=484701.19
Country="Portugal", year=2008, sumValue=624344.12
Country="Portugal", year=2009, sumValue=593993.12
Country="Portugal", year=2010, sumValue=505595.57999999996
Country="Portugal", year=2011, sumValue=560846.59
Country="Portugal", year=2012, sumValue=591193.06
Country="Spain", year=1999, sumValue=28198.07
Country="Spain", year=2000, sumValue=1051966.97
Country="Spain", year=2001, sumValue=1109507.77
Country="Spain", year=2002, sumValue=933743.7000000001
Country="Spain", year=2003, sumValue=749160.5
Country="Spain", year=2004, sumValue=1060027.4300000002
Country="Spain", year=2005, sumValue=964578.5499999999
Country="Spain", year=2006, sumValue=959640.6300000001
Country="Spain", year=2007, sumValue=833432.72
Country="Spain", year=2008, sumValue=1020158.86
Country="Spain", year=2009, sumValue=769760.98
Country="Spain", year=2010, sumValue=907948.0199999999
Country="Spain", year=2011, sumValue=1023883.1399999999
Country="Spain", year=2012, sumValue=916082.9500000001
Country="Sweden", year=1999, sumValue=97148.54000000001
Country="Sweden", year=2000, sumValue=1561702.42
Country="Sweden", year=2001, sumValue=1367386.1600000001
Country="Sweden", year=2002, sumValue=1559736.07
Country="Sweden", year=2003, sumValue=1730958.8100000003
Country="Sweden", year=2004, sumValue=1647124.3400000003
Country="Sweden", year=2005, sumValue=1533781.7800000003
Country="Sweden", year=2006, sumValue=1663473.2300000002
Country="Sweden", year=2007, sumValue=1652737.16
Country="Sweden", year=2008, sumValue=1531570.54
Country="Sweden", year=2009, sumValue=1570810.3099999998
Country="Sweden", year=2010, sumValue=1465015.73
Country="Sweden", year=2011, sumValue=1400147.26
Country="Sweden", year=2012, sumValue=1537604.11
Country="Switzerland", year=1999, sumValue=59855.48
Country="Switzerland", year=2000, sumValue=796839.0599999999
Country="Switzerland", year=2001, sumValue=911520.7899999999
Country="Switzerland", year=2002, sumValue=725821.6000000001
Country="Switzerland", year=2003, sumValue=732879.62
Country="Switzerland", year=2004, sumValue=801696.27
Country="Switzerland", year=2005, sumValue=786072.6599999999
Country="Switzerland", year=2006, sumValue=675051.7799999999
Country="Switzerland", year=2007, sumValue=635044.86
Country="Switzerland", year=2008, sumValue=813917.08
Country="Switzerland", year=2009, sumValue=674287.69
Country="Switzerland", year=2010, sumValue=689898.6799999999
Country="Switzerland", year=2011, sumValue=911731.44
Country="Switzerland", year=2012, sumValue=739135.7799999999
Country="United Kingdom", year=1999, sumValue=211490.34000000003
Country="United Kingdom", year=2000, sumValue=2243676.2199999997
Country="United Kingdom", year=2001, sumValue=2396624.7700000005
Country="United Kingdom", year=2002, sumValue=2384840.43
Country="United Kingdom", year=2003, sumValue=2234803.19
Country="United Kingdom", year=2004, sumValue=2440524.0100000002
Country="United Kingdom", year=2005, sumValue=2417887.1800000006
Country="United Kingdom", year=2006, sumValue=2324619.5100000002
Country="United Kingdom", year=2007, sumValue=2274060.66
Country="United Kingdom", year=2008, sumValue=2475651.1399999997
Country="United Kingdom", year=2009, sumValue=2351097.13
Country="United Kingdom", year=2010, sumValue=2272389.3300000005
Country="United Kingdom", year=2011, sumValue=2291161.93
Country="United Kingdom", year=2012, sumValue=2567453.0599999996
Country="United States", year=1999, sumValue=328894.67
Country="United States", year=2000, sumValue=5205285.260000001
Country="United States", year=2001, sumValue=5480233.43
Country="United States", year=2002, sumValue=4898168.760000001
Country="United States", year=2003, sumValue=5309504.9799999995
Country="United States", year=2004, sumValue=5205069.16
Country="United States", year=2005, sumValue=5218222.55
Country="United States", year=2006, sumValue=5223087.54
Country="United States", year=2007, sumValue=5689281.229999999
Country="United States", year=2008, sumValue=4702522.549999999
Country="United States", year=2009, sumValue=5319394.239999998
Country="United States", year=2010, sumValue=4857114.340000001
Country="United States", year=2011, sumValue=4906520.8100000005
Country="United States", year=2012, sumValue=4925055.0200000005
Country="Venezuela", year=1999, sumValue=275060.99
Country="Venezuela", year=2000, sumValue=2089231.7700000005
Country="Venezuela", year=2001, sumValue=1967789.31
Country="Venezuela", year=2002, sumValue=1891231.77
Country="Venezuela", year=2003, sumValue=2046693.1600000001
Country="Venezuela", year=2004, sumValue=1848997.8699999999
Country="Venezuela", year=2005, sumValue=1526074.82
Country="Venezuela", year=2006, sumValue=2178373.960000001
Country="Venezuela", year=2007, sumValue=2040510.6700000002
Country="Venezuela", year=2008, sumValue=1796885.05
Country="Venezuela", year=2009, sumValue=1910083.8099999998
Country="Venezuela", year=2010, sumValue=2126057.82
Country="Venezuela", year=2011, sumValue=2240743.8499999996
Country="Venezuela", year=2012, sumValue=1529289.48
