package test;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewResult;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import pojo.Order;
import pojo.OrderDetail;

import repositories.OrderRepository;

public class avgUnitPricePerShipperTest {
	
	public static final String[] DAYS = new String[]{"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
	
	public static void main(String[] args) {
		
		System.setProperty("org.ektorp.support.AutoUpdateViewOnChange", "true");
		
		HttpClient couchClient=null;
		try {
			couchClient = new StdHttpClient.Builder()
			.username("admin")
			.password("admin")
			.url("http://localhost:5984")
			.build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		CouchDbInstance dbInstance = new StdCouchDbInstance(couchClient);
		
		CouchDbConnector db = null;
		
		db = dbInstance.createConnector("orders", false);
		
		OrderRepository or = new OrderRepository(db);
		ViewResult result = null;
		
		System.out.println("getAvgUnitPricePerShipper");
		System.out.println("times");
		for(int i=0;i<10;++i){
			long st = System.nanoTime();
			result = or.getAvgUnitPricePerShipper();
			System.out.print(((System.nanoTime()-st)/1000000.0)+";");
		}
		System.out.println();
		for(ViewResult.Row r : result.getRows()){
			JsonNode key = r.getKeyAsNode();
			JsonNode val = r.getValueAsNode();
			
			System.out.println("Shipper id="+key.get(0)+", year="+key.get(1)+", avgUnitPrice="+val.get(0));
		}	
		
	}

}
