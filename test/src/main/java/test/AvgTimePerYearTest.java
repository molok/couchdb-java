package test;

import java.net.MalformedURLException;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewResult;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import repositories.OrderRepository;

public class AvgTimePerYearTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.setProperty("org.ektorp.support.AutoUpdateViewOnChange", "true");
		
		HttpClient couchClient=null;
		try {
			couchClient = new StdHttpClient.Builder()
			.username("admin")
			.password("admin")
			.url("http://localhost:5984")
			.build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		CouchDbInstance dbInstance = new StdCouchDbInstance(couchClient);
		
		CouchDbConnector db = null;
		
		db = dbInstance.createConnector("orders", false);
		OrderRepository or = new OrderRepository(db);	
		ViewResult result = null;
		
		
		System.out.println("getAvgTimePerYear");
		System.out.println("times");
		/*for(int i=0;i<10;++i){
			long st = System.nanoTime();
			result = or.getAvgTimePerYear();
			System.out.print(((System.nanoTime()-st)/1000000.0)+";");
		}*/
		result = iterate(result,or);
		
		System.out.println("\n");
		for(ViewResult.Row row : result.getRows()) {
			System.out.println("Year: "+row.getKey()+", Average time: "+row.getValueAsNode().get(0)+" days");
		}
		
		
		
				
		
	}
	
	private static ViewResult iterate(ViewResult result,OrderRepository or) {
		try {
			for(int i=0;i<10;++i){
				long st = System.nanoTime();
				result = or.getAvgTimePerYear();
				System.out.print(((System.nanoTime()-st)/1000000.0)+";");
			}
			return result;
		} catch(Exception e) {
			return iterate(result,or);
			
		}
	}


}
