package test;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewResult;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import pojo.Product;
import repositories.OrderRepository;

public class QuantityBySupplierTest {

	public static void main(String[] args) {
		System.setProperty("org.ektorp.support.AutoUpdateViewOnChange", "true");

		HttpClient couchClient = null;
		try {
			couchClient = new StdHttpClient.Builder().username("admin")
					.password("admin").url("http://localhost:5984").build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		CouchDbInstance dbInstance = new StdCouchDbInstance(couchClient);

		CouchDbConnector db = null;

		db = dbInstance.createConnector("orders", false);
		OrderRepository or = new OrderRepository(db);

		List<Product> allProd = or.getAllProducts();

		Map<String,Integer> supps = new HashMap<>();
		System.out.println("getQuantityBySupplier");
		System.out.println("times");
		
		ViewResult result = null;
		for(int i=0;i<10;++i){
			long st = System.nanoTime();
			
			result = or.productsWithQuantity();
			
			for (ViewResult.Row row : result.getRows()) {
				
				for(Product p : allProd) {
					if(row.getKey().equals(p.getProductID())) {
						if(supps.containsKey(p.getSupplierID())) {
							int quantity = supps.get(p.getSupplierID());
							quantity += Integer.parseInt(row.getValue());
							supps.put(p.getSupplierID(), quantity);
							break;
						} else {
							supps.put(p.getSupplierID(), Integer.parseInt(row.getValue()));
						}
					}
				}
			}
			System.out.print(((System.nanoTime()-st)/1000000.0)+";");
		}
		
		System.out.println(allProd.size());
		System.out.println(result.getSize());
		for(String supp : supps.keySet()) {
			System.out.println("SupplierId: "+supp+" , quantity: "+supps.get(supp));
		}
	}

}
