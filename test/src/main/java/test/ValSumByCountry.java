package test;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewResult;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import pojo.Order;
import pojo.OrderDetail;

import repositories.OrderRepository;

public class ValSumByCountry {
	
	public static void main(String[] args) {
		
		System.setProperty("org.ektorp.support.AutoUpdateViewOnChange", "true");
		
		HttpClient couchClient=null;
		try {
			couchClient = new StdHttpClient.Builder()
			.username("admin")
			.password("admin")
			.url("http://localhost:5984")
			.build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		CouchDbInstance dbInstance = new StdCouchDbInstance(couchClient);
		
		CouchDbConnector db = null;
		
		db = dbInstance.createConnector("orders", false);
		
		OrderRepository or = new OrderRepository(db);
		ViewResult result = null;
		
		System.out.println("getValueSumByCountry");
		System.out.println("times");
		for(int i=0;i<10;++i){
			long st = System.nanoTime();
			result = or.valueSumByCountry();
			System.out.print(((System.nanoTime()-st)/1000000.0)+";");
		}
		
		System.out.println("\n");
		for(ViewResult.Row r : result.getRows()){
			JsonNode key = r.getKeyAsNode();
			JsonNode val = r.getValueAsNode();
//			System.out.println("Country="+key.get(0)+", year="+key.get(1)+", sumValue="+val.getDoubleValue());
		}
		
		for(int i=0;i<10;++i){
			long st = System.nanoTime();
			List<Order> allOrders = or.getAllOrders();
			Map<String,Map<String,Double>> data = new HashMap<>();
			for(Order o : allOrders){
				String country = o.getShipCountry();
				String year = o.getShippedDate().substring(0, 4);
				double valueSum = 0.0;
				for(OrderDetail od : o.getDetails()){
					valueSum += od.getQuantity()*od.getUnitPrice();
				}			
				Map<String,Double> inner = data.get(country);
				if(inner!=null){
					Double val = inner.get(year);
					if(val!=null){
						val+=valueSum;
					}
					else{
						inner.put(year, val);
					}
				}
				else{
					inner = new HashMap<>();
					inner.put(year, valueSum);
					data.put(country, inner);
				}
			}
			System.out.print(((System.nanoTime()-st)/1000000.0)+";");
		}
		
	}

}
