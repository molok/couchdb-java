package test;

import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;
import org.pojoxml.core.PojoXml;
import org.pojoxml.core.PojoXmlFactory;

import pojo.Category;
import pojo.Customer;
import pojo.Employee;
import pojo.Order;
import pojo.OrderDetail;
import pojo.Product;
import pojo.Shipper;
import pojo.Supplier;
import tables.Categories;
import tables.Customers;
import tables.Employees;
import tables.OrderDetails;
import tables.Orders;
import tables.Products;
import tables.Shippers;
import tables.Suppliers;


public class LoadingTest {

	public static void main(String[] args) {
		
		PojoXml pojo = PojoXmlFactory.createPojoXml();
		
		Shippers shippers = (Shippers) pojo.getPojoFromFile(".\\sampledata\\shippers.xml", Shippers.class);
		Suppliers suppliers = (Suppliers) pojo.getPojoFromFile(".\\sampledata\\suppliers.xml", Suppliers.class);
		Categories categories = (Categories) pojo.getPojoFromFile(".\\sampledata\\categories.xml", Categories.class);
		Customers customers = (Customers) pojo.getPojoFromFile(".\\sampledata\\customers.xml", Customers.class);
		Employees employees = (Employees) pojo.getPojoFromFile(".\\sampledata\\employees.xml", Employees.class);
		OrderDetails orderdetails = (OrderDetails) pojo.getPojoFromFile(".\\sampledata\\orderdetails_rand_10000.xml", OrderDetails.class);
		Orders orders = (Orders) pojo.getPojoFromFile(".\\sampledata\\orders_rand_10000.xml", Orders.class);
		OrderDetails orderdetails2 = (OrderDetails) pojo.getPojoFromFile(".\\sampledata\\orderdetails_rand_20000.xml", OrderDetails.class);
		Orders orders2 = (Orders) pojo.getPojoFromFile(".\\sampledata\\orders_rand_20000.xml", Orders.class);
		Products products = (Products) pojo.getPojoFromFile(".\\sampledata\\products.xml", Products.class);
		
		System.out.println("Data loaded from files :");
		System.out.println("Shippers : "+shippers.getShipper().length);
		System.out.println("Suppliers : "+suppliers.getSupplier().length);
		System.out.println("Categories : "+categories.getCategory().length);
		System.out.println("Customers : "+customers.getCustomer().length);
		System.out.println("Employees : "+employees.getEmployee().length);
		System.out.println("OrderDetails : "+orderdetails.getOrderdetail().length);
		System.out.println("Orders : "+orders.getOrder().length);
		System.out.println("OrderDetails2 : "+orderdetails2.getOrderdetail().length);
		System.out.println("Orders2 : "+orders2.getOrder().length);
		System.out.println("Products : "+products.getProduct().length);
		
		//join details with order
		joinDetails(orderdetails, orders);
		joinDetails(orderdetails2, orders2);
		
		System.out.println("Data prepared.");
		
		//put things in database
		HttpClient couchClient=null;
		try {
			couchClient = new StdHttpClient.Builder()
			.username("admin")
			.password("admin")
			.url("http://localhost:5984")
			.build();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		CouchDbInstance dbInstance = new StdCouchDbInstance(couchClient);
		//cleanup
		cleanUp(dbInstance);
		
		CouchDbConnector db = null;
		
//		System.out.println("data loading (1 write per document)");
//		for(int i=0;i<5;++i){
//			System.out.println("test "+(i+1));
//			cleanUp(dbInstance);
//			cleanRevOrders(orders);
//			cleanRevOrders(orders2);
//			db = dbInstance.createConnector("orders", true);			
//			testOrdersLoad(orders, db);
//			testOrdersLoad(orders2, db);			
//		}
		
		db = dbInstance.createConnector("orders", true);
		
		System.out.println("data loading (bulk - 1000 documents in write)");
		for(int i=0;i<5;++i){
			System.out.println("test "+(i+1));
			cleanUp(dbInstance);
			cleanRevOrders(orders);
			cleanRevOrders(orders2);
			db = dbInstance.createConnector("orders", true);			
			testOrdersBulkLoad(orders, db);
			testOrdersBulkLoad(orders2, db);	
			System.out.println("\n");
		}
		
//		db = dbInstance.createConnector("suppliers", true);
		for(Supplier s : suppliers.getSupplier()){
			db.create(s);
		}
		
//		db = dbInstance.createConnector("shippers", true);		
		for(Shipper s : shippers.getShipper()){
			db.create(s);
		}	
		
//		db = dbInstance.createConnector("categories", true);
		for(Category c : categories.getCategory()){
			db.create(c);
		}
		
//		db = dbInstance.createConnector("customers", true);
		for(Customer c : customers.getCustomer()){
			db.create(c);
		}
//		db = dbInstance.createConnector("employees", true);
		for(Employee e : employees.getEmployee()){
			db.create(e);
		}
		
//		db = dbInstance.createConnector("products", true);
		for(Product p : products.getProduct()){
			db.create(p);
		}
		
	}
	
	public static void cleanRevOrders(Orders orders){
		for(Order o : orders.getOrder()){
			o.setRevision(null);
		}
	}
	public static void testOrdersLoad(Orders orders, CouchDbConnector db){
		int i=0;
		DecimalFormat df = new DecimalFormat("0.00");
		
		long elapsedTime=0;
		double elTime = 0.0;		
		long startTime = System.nanoTime();
		for(Order o : orders.getOrder()){
			if(i<1000){
				db.create(o);
				++i;
			}
			else{
				elapsedTime = System.nanoTime() - startTime;
				elTime = elapsedTime;
				System.out.print(df.format((1000.0/(elTime/1000000000.0)))+";");
				startTime = System.nanoTime();
				i=0;
			}
		}
		elapsedTime = System.nanoTime() - startTime;
		elTime = elapsedTime;
		System.out.println(df.format(1000.0/(elTime/1000000000.0)));		
	}
	
	public static void testOrdersBulkLoad(Orders orders, CouchDbConnector db){
		
		DecimalFormat df = new DecimalFormat("0.00");		
		long elapsedTime=0;
		double elTime = 0.0;
		long startTime = System.nanoTime();
		Order[] ord = orders.getOrder();		
		int loops = ord.length/1000;
		for(int i=0;i<loops;++i){
			Order[] tmp = Arrays.copyOfRange(ord, i*1000, (i+1)*1000);
			List<Order> bulkData = Arrays.asList(tmp);
			startTime = System.nanoTime();
			db.executeBulk(bulkData);
			elapsedTime = System.nanoTime() - startTime;
			elTime = elapsedTime;
			System.out.print(df.format((1000.0/(elTime/1000000000.0)))+";");
		}	
	}

	public static void cleanUp(CouchDbInstance dbInstance) {
		try {
			dbInstance.deleteDatabase("shippers");
		} catch (Exception e) {/* eating exception */}
		try {
			dbInstance.deleteDatabase("suppliers");
		} catch (Exception e) {/* eating exception */}
		try {
			dbInstance.deleteDatabase("orders");
		} catch (Exception e) {/* eating exception */}
		try {
			dbInstance.deleteDatabase("products");
		} catch (Exception e) {/* eating exception */}
		try {
			dbInstance.deleteDatabase("employees");
		} catch (Exception e) {/* eating exception */}
		try {
			dbInstance.deleteDatabase("customers");
		} catch (Exception e) {/* eating exception */}
		try {
			dbInstance.deleteDatabase("categories");
		} catch (Exception e) {/* eating exception */}
	}
	
	public static void joinDetails(OrderDetails details, Orders orders){
		for(OrderDetail od : details.getOrderdetail()){
			String id = od.getOrderID();
			for(Order o : orders.getOrder()){
				if(o.getOrderID().equals(id)){
					o.getDetails().add(od);
				}
			}
		}
	}

}