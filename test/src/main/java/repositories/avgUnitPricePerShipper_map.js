function(doc) {
	if(doc.type=="Order"){
		var d = (new Date(doc.shippedDate)).getFullYear()
		doc.details.forEach(function(e,i,a){
			emit([doc.shipVia,d],e.unitPrice)
		})
	}
}