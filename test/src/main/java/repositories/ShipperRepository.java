package repositories;

import org.ektorp.CouchDbConnector;
import org.ektorp.ViewResult;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.View;
import org.ektorp.support.Views;

import pojo.Shipper;

@Views({
	@View( name = "all", map = "function(doc) { emit(null,doc._id)}"),
	@View( name = "id_sum", map = "function(doc) { emit(null,doc._id)}", 
		reduce = "function(keys,values) {return sum(values)}"),
	@View( name = "random", map = "function(doc) { emit(doc._id, {CompanyName : doc.companyName, Phone : doc.phone})}")
})
public class ShipperRepository extends CouchDbRepositorySupport<Shipper> {

	public ShipperRepository(CouchDbConnector db) {
		super(Shipper.class,db);
		initStandardDesignDocument();
	}
	
	public int getIdSum() {
		ViewResult r = db.queryView(createQuery("id_sum"));
        return r.getRows().get(0).getValueAsInt();
	}

}
