function(doc) {
	if(doc.type=="Order"){
		var d = (new Date(doc.shippedDate)).getDay()
		doc.details.forEach(function(e,i,a){
			emit(d,e.unitPrice*e.quantity)
		})
	}
}