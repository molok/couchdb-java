package repositories;

import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.ViewQuery;
import org.ektorp.ViewResult;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.View;
import org.ektorp.support.Views;

import pojo.Order;
import pojo.Product;

@Views({
	@View(name = "perCountry", map = "classpath:ordersPerCountry_map.js",
			reduce = "classpath:ordersPerCountry_reduce.js"),
			
	@View(name = "avgUnitPricePerShipper",
	map="classpath:avgUnitPricePerShipper_map.js",
	reduce="classpath:avgUnitPricePerShipper_reduce.js"),
	
	@View(name = "valueSumByCountry",
	map="classpath:valueSumByCountry_map.js",
	reduce="_sum"),
	
	@View(name = "valueByWeekDay",
	map="classpath:valueByWeekDay_map.js",
	reduce="_sum"),
	
	@View( name = "avgTimePerYear", map="classpath:avgTimePerYear_map.js",
	reduce = "classpath:avgTimePerYear_reduce.js"),
	
	@View( name = "productsWithQuantity",
	map = "function(doc) {"+
		"if (doc.type == \"Order\") {"+
			"doc.details.forEach(function(e,i,a){"+
				"emit(e.productID,e.quantity)"+
			"})"+
		"}"+
	"}",
	reduce="_sum"
	),
	
	@View( name = "allProducts",map="function(doc){if(doc.type==\"Product\"){emit(doc.supplierID,doc)}}" ),
	
	@View( name = "allOrders",map="function(doc){if(doc.type==\"Order\"){emit(null,doc)}}" )
	
})
public class OrderRepository extends CouchDbRepositorySupport<Order> {

	public OrderRepository(CouchDbConnector db) {
		super(Order.class,db);
		initStandardDesignDocument();
	}
	
	public ViewResult productsWithQuantity() {
		ViewQuery query = new ViewQuery()
			.viewName("productsWithQuantity")
			.designDocId("_design/Order")
			.group(true);
	    return db.queryView(query);
	}
	
	public List<Order> getAllOrders() {
		ViewQuery query = new ViewQuery()
			.viewName("allOrders")
			.designDocId("_design/Order");
	    return db.queryView(query,Order.class);
	}
	
	public List<Product> getAllProducts() {
		ViewQuery query = new ViewQuery()
			.viewName("allProducts")
			.designDocId("_design/Order");
	    return db.queryView(query,Product.class);
	}
	
	public ViewResult valueByWeekDay() {
		ViewQuery query = new ViewQuery()
			.viewName("valueByWeekDay")
			.designDocId("_design/Order")
			.group(true);
	    return db.queryView(query);
	}
	
	public ViewResult valueSumByCountry() {
		ViewQuery query = new ViewQuery()
			.viewName("valueSumByCountry")
			.designDocId("_design/Order")
			.group(true);
	    return db.queryView(query);
	}
	
	public ViewResult getAvgUnitPricePerShipper() {
		ViewQuery query = new ViewQuery()
			.viewName("avgUnitPricePerShipper")
			.designDocId("_design/Order")
			.group(true);
	    return db.queryView(query);
	}
	
	public ViewResult getOrdersPerCountry() {
		ViewQuery query = new ViewQuery()
			.viewName("perCountry")
			.designDocId("_design/Order")
			.group(true);
		return db.queryView(query);
	}
	
	public ViewResult getAvgTimePerYear() {
		ViewQuery query = new ViewQuery()
		.viewName("avgTimePerYear")
		.designDocId("_design/Order")
		.group(true);
    return db.queryView(query);
	}

	

}
