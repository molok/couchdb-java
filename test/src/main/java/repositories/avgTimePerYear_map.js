function(doc) {
	if(doc.type=="Order"){
		var startDate = new Date(doc.orderDate);
		var endDate = new Date(doc.shippedDate);
		var year = endDate.getFullYear();
		emit(year,((endDate.getTime()-startDate.getTime())/1000/3600/24));
	}
}