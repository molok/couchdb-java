package tables;

import pojo.OrderDetail;

public class OrderDetails {
	private OrderDetail[] orderdetail;

	public OrderDetail[] getOrderdetail() {
		return orderdetail;
	}

	public void setOrderdetail(OrderDetail[] orderdetail) {
		this.orderdetail = orderdetail;
	}
}
