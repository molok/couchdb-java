package pojo;

import java.util.ArrayList;
import java.util.List;

import org.ektorp.support.CouchDbDocument;

public class Order extends CouchDbDocument{

	private String OrderID;
	private String CustomerID;
	private String EmployeeID;
	private String OrderDate;
	private String RequiredDate;
	private String ShippedDate;
	private String ShipVia;
	private int Freight;
	private String ShipName;
	private String ShipAddress;
	private String ShipCity;
	private String ShipPostalCode;
	private String ShipCountry;
	private final String type = "Order";
	
	public String getType() {
		return type;
	}
	private List<OrderDetail> details = new ArrayList<>();
	
	public List<OrderDetail> getDetails() {
		return details;
	}
	public void setDetails(List<OrderDetail> details) {
		this.details = details;
	}
	public String getOrderID() {
		return OrderID;
	}
	public void setOrderID(String orderID) {
		OrderID = orderID;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public String getOrderDate() {
		return OrderDate;
	}
	public void setOrderDate(String orderDate) {
		OrderDate = orderDate;
	}
	public String getRequiredDate() {
		return RequiredDate;
	}
	public void setRequiredDate(String requiredDate) {
		RequiredDate = requiredDate;
	}
	public String getShippedDate() {
		return ShippedDate;
	}
	public void setShippedDate(String shippedDate) {
		ShippedDate = shippedDate;
	}
	public String getEmployeeID() {
		return EmployeeID;
	}
	public void setEmployeeID(String employeeID) {
		EmployeeID = employeeID;
	}
	public String getShipVia() {
		return ShipVia;
	}
	public void setShipVia(String shipVia) {
		ShipVia = shipVia;
	}
	public int getFreight() {
		return Freight;
	}
	public void setFreight(int freight) {
		Freight = freight;
	}
	public String getShipName() {
		return ShipName;
	}
	public void setShipName(String shipName) {
		ShipName = shipName;
	}
	public String getShipAddress() {
		return ShipAddress;
	}
	public void setShipAddress(String shipAddress) {
		ShipAddress = shipAddress;
	}
	public String getShipCity() {
		return ShipCity;
	}
	public void setShipCity(String shipCity) {
		ShipCity = shipCity;
	}
	public String getShipPostalCode() {
		return ShipPostalCode;
	}
	public void setShipPostalCode(String shipPostalCode) {
		ShipPostalCode = shipPostalCode;
	}
	public String getShipCountry() {
		return ShipCountry;
	}
	public void setShipCountry(String shipCountry) {
		ShipCountry = shipCountry;
	}
}
