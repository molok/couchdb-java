package pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.ektorp.support.CouchDbDocument;

public class Product  extends CouchDbDocument{
	private String ProductID;
	private String ProductName;
	private String SupplierID;
	private String CategoryID;
	private String QuantityPerUnit;
	private double UnitPrice;
	private int UnitsInStock;
	private int UnitsOnOrder;
	private int ReorderLevel;
	private boolean Discontinued;
	private final String type = "Product";	
	
	public String getType() {
		return type;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getProductID() {
		return ProductID;
	}
	public void setProductID(String productID) {
		ProductID = productID;
	}
	public String getSupplierID() {
		return SupplierID;
	}
	public void setSupplierID(String supplierID) {
		SupplierID = supplierID;
	}
	public String getCategoryID() {
		return CategoryID;
	}
	public void setCategoryID(String categoryID) {
		CategoryID = categoryID;
	}
	public String getQuantityPerUnit() {
		return QuantityPerUnit;
	}
	public void setQuantityPerUnit(String quantityPerUnit) {
		QuantityPerUnit = quantityPerUnit;
	}
	public double getUnitPrice() {
		return UnitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		UnitPrice = unitPrice;
	}
	public int getUnitsInStock() {
		return UnitsInStock;
	}
	public void setUnitsInStock(int unitsInStock) {
		UnitsInStock = unitsInStock;
	}
	public int getUnitsOnOrder() {
		return UnitsOnOrder;
	}
	public void setUnitsOnOrder(int unitsOnOrder) {
		UnitsOnOrder = unitsOnOrder;
	}
	public int getReorderLevel() {
		return ReorderLevel;
	}
	public void setReorderLevel(int reorderLevel) {
		ReorderLevel = reorderLevel;
	}
	public boolean isDiscontinued() {
		return Discontinued;
	}
	public void setDiscontinued(boolean discontinued) {
		Discontinued = discontinued;
	}
	
	
}
