package pojo;

import org.ektorp.support.CouchDbDocument;


public class Shipper extends CouchDbDocument{

	private String ShipperID;
	private String CompanyName;
	private String Phone;
	private final String type="Shipper";

	public String getType() {
		return type;
	}
	public String getShipperID() {
		return ShipperID;
	}
	public void setShipperID(String shipperID) {
		ShipperID = shipperID;
	}
	
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
}
