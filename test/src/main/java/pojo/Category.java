package pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.ektorp.support.CouchDbDocument;

public class Category extends CouchDbDocument{
	
	private String CategoryID;
	private String CategoryName;
	private String Description;
	private String rev;
	private final String type = "Category";
	
	public String getType() {
		return type;
	}
	
	public String getCategoryID() {
		return CategoryID;
	}
	
	public void setCategoryID(String categoryID) {
		CategoryID = categoryID;
	}
	public String getCategoryName() {
		return CategoryName;
	}
	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}

}
